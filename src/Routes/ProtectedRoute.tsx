import React from 'react';
import { Redirect, Route } from 'react-router-dom';

export default function ProtectedRoute({ isAuthenticated, ...props }: any) {
     
    return !isAuthenticated ?
        (
            <Redirect to= '/' />
        ):(
            <Route {...props} />
        )
}
