import React from 'react'

export default function WindowSize() {
    const [size, setSize] = React.useState([window.innerHeight, window.innerWidth]);
  
    React.useEffect(() => {
      const handleResize = () => {
        setSize([window.innerHeight, window.innerWidth]);
      };
      window.addEventListener('resize', handleResize);
    }, [])
    return size;
}
