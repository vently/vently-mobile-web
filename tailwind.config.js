module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
      'heebo-regular': 'Heebo-regular',
        'heebo-medium': 'Heebo-medium',
        'heebo-bold': 'Heebo-bold',
        'poppins-regular': 'Poppins-regular',
        'poppins-medium': 'Poppins-medium',
        'poppins-bold': ['Poppins-bold'],
        'abeat': 'Abeatbykai',
        'rubik-bold' : 'Rubik-bold',
        'rubik-light' : 'Rubik-light',
        'rubik-medium' : 'Rubik-medium',
        'rubik-regular' : 'Rubik-regular',
      },
      color: {
        ventlyRed: '#FF4471',
      },
      backgroundColor: {
        primary: "var(--color-bg-primary)",
        secondary: "var(--color-bg-secondary)",
        active: "var(--color-bg-active)",
        notactive: "var(--color-bg-notactive)",
        carousel: "var(--color-bg-carousel)", 
      },
      textColor: {
        accent: "var(--color-text-accent)",
        primary: "var(--color-text-primary)",
        secondary: "var(--color-text-secondary)",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
